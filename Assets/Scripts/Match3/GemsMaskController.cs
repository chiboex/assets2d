using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemsMaskController : MonoBehaviour
{
    private Match3BoardController boardController;
    // Start is called before the first frame update
    void Start()
    {
        boardController = FindObjectOfType<Match3BoardController>();

        transform.localScale = new Vector3(boardController.boardWidth + .25f, boardController.boardHeight + .25f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
